package pl.javastart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDeployCiJenkinsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDeployCiJenkinsApplication.class, args);
	}

}
